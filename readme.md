## run nextcloud a pelo
docker run -d -p 4443:4443 -p 443:443 -p 80:80 -v /mnt/md0/data:/data --name nextcloudpi ownyourbits/nextcloudpi 10.1.1.201

## con docker-compose
```
cd /home/oliet/nextcloud
docker-compose up -d
```

## install
copy all files to /home/oliet/nextcloud
run install-service.sh